﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WA.Server.Data;
using WA.Shared.Entities;

namespace WA.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PrefecturesController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public PrefecturesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Prefectures
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PrefectureEntity>>> GetPrefectures()
        {
            try
            {
                return await _context.Prefectures
                    .Include(p => p.Region)
                    .OrderBy(p => p.PrefectureId)
                    .ToListAsync();
            }
            catch (Exception)
            {
                throw;
            }
        }

        // GET: api/Prefectures/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PrefectureEntity>> GetPrefectureEntity(int id)
        {
            var prefectureEntity = await _context.Prefectures.FindAsync(id);

            if (prefectureEntity == null)
            {
                return NotFound();
            }

            return prefectureEntity;
        }

        // PUT: api/Prefectures/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPrefectureEntity(int id, PrefectureEntity prefectureEntity)
        {
            if (id != prefectureEntity.PrefectureId)
            {
                return BadRequest();
            }

            _context.Entry(prefectureEntity).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PrefectureEntityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Prefectures
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<PrefectureEntity>> PostPrefectureEntity(PrefectureEntity prefectureEntity)
        {
            _context.Prefectures.Add(prefectureEntity);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetPrefectureEntity", new { id = prefectureEntity.PrefectureId }, prefectureEntity);
        }

        // DELETE: api/Prefectures/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePrefectureEntity(int id)
        {
            var prefectureEntity = await _context.Prefectures.FindAsync(id);
            if (prefectureEntity == null)
            {
                return NotFound();
            }

            _context.Prefectures.Remove(prefectureEntity);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool PrefectureEntityExists(int id)
        {
            return _context.Prefectures.Any(e => e.PrefectureId == id);
        }
    }
}