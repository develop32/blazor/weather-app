﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WA.Server.Data;
using WA.Shared.Entities;

namespace WA.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegionsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public RegionsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Regions
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RegionEntity>>> GetRegions()
        {
            return await _context.Regions.OrderBy(r => r.RegionId).ToListAsync();
        }

        // GET: api/Regions/5
        [HttpGet("{id}")]
        public async Task<ActionResult<RegionEntity>> GetRegionEntity(int id)
        {
            var regionEntity = await _context.Regions.FindAsync(id);

            if (regionEntity == null)
            {
                return NotFound();
            }

            return regionEntity;
        }

        // PUT: api/Regions/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRegionEntity(int id, RegionEntity regionEntity)
        {
            if (id != regionEntity.RegionId)
            {
                return BadRequest();
            }

            _context.Entry(regionEntity).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RegionEntityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Regions
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<RegionEntity>> PostRegionEntity(RegionEntity regionEntity)
        {
            _context.Regions.Add(regionEntity);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetRegionEntity", new { id = regionEntity.RegionId }, regionEntity);
        }

        // DELETE: api/Regions/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteRegionEntity(int id)
        {
            var regionEntity = await _context.Regions.FindAsync(id);
            if (regionEntity == null)
            {
                return NotFound();
            }

            _context.Regions.Remove(regionEntity);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool RegionEntityExists(int id)
        {
            return _context.Regions.Any(e => e.RegionId == id);
        }
    }
}