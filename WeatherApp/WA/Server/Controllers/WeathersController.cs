﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WA.Server.Data;
using WA.Shared.Entities;
using WA.Shared.ValueObjects;

namespace WA.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WeathersController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public WeathersController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Weathers
        [HttpGet]
        public async Task<ActionResult<IEnumerable<WeatherEntity>>> GetWeathers(string regionId = "0", string prefectureId = "0")
        {
            //return await _context.Weathers.Include(w => w.Prefecture.Region).ToListAsync();
            Console.WriteLine($"regionId={regionId}, prefectureId={prefectureId}\n");
            int regionId_Int = int.Parse(regionId);
            int prefectureId_Int = int.Parse(prefectureId);

            return await _context.Weathers
                        .Include(w => w.Prefecture.Region)
                        .Where(w => regionId_Int == 0 || w.Prefecture.RegionId.Equals(regionId_Int))
                        .Where(w => prefectureId_Int == 0 || w.PrefectureId.Equals(prefectureId_Int))
                        .ToListAsync();
        }

        //[HttpGet("{regionId}/{pregectureId}")]
        //public async Task<ActionResult<IEnumerable<WeatherEntity>>> GetWeathersBy(int regionId, int prefectureId)
        //{
        //    return await _context.Weathers
        //        .Include(w => w.Prefecture.Region)
        //        .Where(w => regionId == 0 || w.Prefecture.RegionId.Equals(regionId))
        //        .Where(w => prefectureId == 0 || w.PrefectureId.Equals(prefectureId))
        //        .ToListAsync();
        //}

        // GET: api/Weathers/5
        [HttpGet("{id}")]
        public async Task<ActionResult<WeatherEntity>> GetWeatherEntity(int id)
        {
            var weather = await _context.Weathers
                                .Include(w => w.Prefecture.Region)
                                .Where(w => w.WeatherId == id)
                                .FirstOrDefaultAsync();

            if (weather == null)
            {
                return NotFound();
            }

            return weather;
        }

        // PUT: api/Weathers/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutWeatherEntity(int id, WeatherEntity weather)
        {
            if (id != weather.WeatherId)
            {
                return BadRequest();
            }

            var updateWeather = await _context.Weathers
                                        .Where(w => w.WeatherId == id)
                                        .FirstOrDefaultAsync();
            if (updateWeather == null)
            {
                return NotFound();
            }
            updateWeather.PrefectureId = weather.PrefectureId;
            updateWeather.WeatherDate = weather.WeatherDate;
            updateWeather.Condition.Value = weather.Condition.Value;
            updateWeather.Temperature.Value = weather.Temperature.Value;
            updateWeather.UpdatedOn = DateTime.Now;
            _context.Entry(updateWeather).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!WeatherExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Weathers
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<WeatherEntity>> PostWeatherEntity(WeatherEntity weather)
        {
            try
            {
                Debug.WriteLine("---------------------------------");
                WeatherEntity createdWeather = new()
                {
                    PrefectureId = weather.PrefectureId,
                    WeatherDate = new WeatherDate(weather.WeatherDate.Value),
                    Condition = new Condition(weather.Condition.Value),
                    Temperature = new Temperature(weather.Temperature.Value),
                    CreatedBy = 0,
                    CreatedOn = DateTime.Now,
                    UpdatedBy = 0,
                    UpdatedOn = DateTime.Now,
                };

                Console.WriteLine($"【createdWeather】");
                Console.WriteLine($"PrefectureId={createdWeather.PrefectureId}");
                Console.WriteLine($"WeatherDate={createdWeather.WeatherDate.Value}");
                Console.WriteLine($"Condition={createdWeather.Condition.Value}");
                Console.WriteLine($"Temperature={createdWeather.Temperature.Value}");

                await _context.Weathers.AddAsync(createdWeather);
                await _context.SaveChangesAsync();

                return CreatedAtAction("GetWeatherEntity", new { id = createdWeather.WeatherId }, createdWeather);
            }
            catch (Exception)
            {
                throw;
            }
        }

        // DELETE: api/Weathers/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteWeatherEntity(int id)
        {
            var weatherEntity = await _context.Weathers.FindAsync(id);
            if (weatherEntity == null)
            {
                return NotFound();
            }

            _context.Weathers.Remove(weatherEntity);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool WeatherExists(int id)
        {
            return _context.Weathers.Any(e => e.WeatherId == id);
        }
    }
}