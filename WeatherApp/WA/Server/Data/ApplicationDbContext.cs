﻿using Microsoft.EntityFrameworkCore;
using WA.Shared.Entities;

#nullable disable

namespace WA.Server.Data
{
    public partial class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext()
        {
        }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<PrefectureEntity> Prefectures { get; set; }
        public virtual DbSet<RegionEntity> Regions { get; set; }
        public virtual DbSet<WeatherEntity> Weathers { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("FREE")
                .HasAnnotation("Relational:Collation", "USING_NLS_COMP");

            modelBuilder.Entity<PrefectureEntity>(entity =>
            {
                entity.Property(e => e.PrefectureId).HasPrecision(10);

                entity.Property(e => e.CreatedBy)
                    .HasPrecision(10)
                    .HasDefaultValueSql("0 ");

                entity.Property(e => e.CreatedOn).HasDefaultValueSql("SYSDATE ");

                entity.Property(e => e.RegionId).HasPrecision(10);

                entity.Property(e => e.UpdatedBy)
                    .HasPrecision(10)
                    .HasDefaultValueSql("0 ");

                entity.Property(e => e.UpdatedOn).HasDefaultValueSql("SYSDATE ");

                entity.HasOne(d => d.Region)
                    .WithMany(p => p.Prefectures)
                    .HasForeignKey(d => d.RegionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Prefectures_FK1");
            });

            modelBuilder.Entity<RegionEntity>(entity =>
            {
                entity.Property(e => e.RegionId).HasPrecision(10);

                entity.Property(e => e.CreatedBy)
                    .HasPrecision(10)
                    .HasDefaultValueSql("0 ");

                entity.Property(e => e.CreatedOn).HasDefaultValueSql("SYSDATE ");

                entity.Property(e => e.UpdatedBy)
                    .HasPrecision(10)
                    .HasDefaultValueSql("0 ");

                entity.Property(e => e.UpdatedOn).HasDefaultValueSql("SYSDATE ");
            });

            modelBuilder.Entity<WeatherEntity>(entity =>
            {
                entity.Property(e => e.WeatherId).HasPrecision(10);

                entity.Property(e => e.CreatedBy)
                    .HasPrecision(10)
                    .HasDefaultValueSql("0 ");

                entity.Property(e => e.CreatedOn).HasDefaultValueSql("SYSDATE ");

                entity.Property(e => e.PrefectureId).HasPrecision(10);

                entity.Property(e => e.UpdatedBy)
                    .HasPrecision(10)
                    .HasDefaultValueSql("0 ");

                entity.Property(e => e.UpdatedOn).HasDefaultValueSql("SYSDATE ");

                entity.OwnsOne(w => w.Condition, c => c.Property(x => x.Value).HasPrecision(1));
                entity.OwnsOne(w => w.WeatherDate, d => d.Property(x => x.Value));
                entity.OwnsOne(w => w.Temperature, t => t.Property(x => x.Value));

                entity.HasOne(d => d.Prefecture)
                    .WithMany(p => p.Weathers)
                    .HasForeignKey(d => d.PrefectureId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Weathers_FK1");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}