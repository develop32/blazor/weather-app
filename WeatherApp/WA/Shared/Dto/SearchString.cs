﻿namespace WA.Shared.Dto
{
    /// <summary>
    /// 検索文字
    /// </summary>
    public class SearchString
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SearchString"/> class.
        /// </summary>
        /// <param name="regionId">地域ID</param>
        /// <param name="prefectureId">都道府県ID</param>
        public SearchString(int regionId, int prefectureId)
        {
            RegionId = regionId;
            PrefectureId = prefectureId;
        }

        /// <summary>
        /// 地域ID
        /// </summary>
        public int RegionId { get; set; }

        /// <summary>
        /// 都道府県ID
        /// </summary>
        public int PrefectureId { get; set; }
    }
}