﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace WA.Shared.Entities
{
    /// <summary>
    /// エンティティクラスの基底クラス。
    /// エンティティクラス共通のプロパティ。
    /// </summary>
    public abstract class AbstractEntity
    {
        /// <summary>
        /// 作成者の取得または設定をします。
        /// </summary>
        public int CreatedBy { get; set; }

        /// <summary>
        /// 作成日時の取得または設定をします。
        /// </summary>
        [Column(TypeName = "DATE")]
        public DateTime CreatedOn { get; set; }

        /// <summary>
        /// 更新者の取得または設定をします。
        /// </summary>
        public int UpdatedBy { get; set; }

        /// <summary>
        /// 更新日時の取得または設定をします。
        /// </summary>
        [Column(TypeName = "DATE")]
        public DateTime UpdatedOn { get; set; }
    }
}