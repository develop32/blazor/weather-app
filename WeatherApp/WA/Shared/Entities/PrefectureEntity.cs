﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace WA.Shared.Entities
{
    /// <summary>
    /// 都道府県エンティティクラス。
    /// DB上のテーブル「都道府県(Prefecture)」のレコード(1行)が格納されます。<br />
    /// また、紐づく関連テーブル「地方(Regions)」、「天気(Weathers)」の情報も格納されます。
    /// </summary>
    public partial class PrefectureEntity : AbstractEntity
    {
        /// <summary>
        /// コンストラクタ。
        /// <see cref="PrefectureEntity"/>クラスの新しいインスタンスを初期化します。
        /// </summary>
        public PrefectureEntity()
        {
            this.Weathers = new HashSet<WeatherEntity>();
        }

        /// <summary>
        /// 都道府県IDの取得または設定をします。
        /// </summary>
        [Key]
        public int PrefectureId { get; set; }

        /// <summary>
        /// 地方IDの取得または設定をします。<br />
        /// 外部キーです。
        /// </summary>
        [Column("RegionID")]
        public int RegionId { get; set; }

        /// <summary>
        /// 都道府県名の取得または設定をします。<br />
        /// </summary>
        [Required]
        [StringLength(100)]
        public string PrefectureName { get; set; }

        /// <summary>
        /// 地方の取得または設定をします。
        /// </summary>
        [ForeignKey(nameof(RegionId))]
        [InverseProperty("Prefectures")]
        public virtual RegionEntity Region { get; set; }

        /// <summary>
        /// 天気の取得または設定をします。
        /// </summary>
        [InverseProperty(nameof(WeatherEntity.Prefecture))]
        public virtual ICollection<WeatherEntity> Weathers { get; set; }
    }
}