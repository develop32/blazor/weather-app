﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

#nullable disable

namespace WA.Shared.Entities
{
    /// <summary>
    /// 地方エンティティクラス。
    /// DB上のテーブル「地方(Regions)」のレコード(1行)が格納されます。<br />
    /// また、紐づく関連テーブル「都道府県(Prefecture)」の情報も格納されます。
    /// </summary>
    public partial class RegionEntity : AbstractEntity
    {
        /// <summary>
        /// コンストラクタ。
        /// <see cref="RegionEntity"/>クラスの新しいインスタンスを初期化します。
        /// </summary>
        public RegionEntity()
        {
            this.Prefectures = new HashSet<PrefectureEntity>();
        }

        /// <summary>
        /// 地方IDの取得または設定をします。
        /// </summary>
        [Key]
        [Column("RegionID")]
        public int RegionId { get; set; }

        /// <summary>
        /// 地方名の取得または設定をします。
        /// </summary>
        [Required]
        [StringLength(100)]
        public string RegionName { get; set; }

        /// <summary>
        /// 都道府県の取得または設定をします。
        /// </summary>
        [InverseProperty(nameof(PrefectureEntity.Region))]
        public virtual ICollection<PrefectureEntity> Prefectures { get; set; }
    }
}