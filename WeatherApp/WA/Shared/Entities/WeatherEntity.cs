﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using WA.Shared.ValueObjects;

namespace WA.Shared.Entities
{
    /// <summary>
    /// 天気エンティティクラス。
    /// DB上のテーブル「天気(Weathers)」のレコード(1行)が格納されます。<br />
    /// また、紐づく関連テーブル「都道府県(Prefecture)」の情報も格納されます。
    /// </summary>
    [Index(nameof(PrefectureId), nameof(WeatherDate), Name = "Weathers_IX1", IsUnique = true)]
    public partial class WeatherEntity : AbstractEntity
    {
        /// <summary>
        /// 天気IDの取得または設定をします。
        /// </summary>
        [Key]
        public int WeatherId { get; set; }

        /// <summary>
        /// 都道府県IDの取得または設定をします。
        /// </summary>
        [Required]
        public int PrefectureId { get; set; }

        /// <summary>
        /// 天気日付の取得または設定をします。
        /// </summary>
        /// <seealso cref="WeatherDate"/>
        public WeatherDate WeatherDate { get; set; }

        /// <summary>
        /// 状態の取得または設定をします。
        /// </summary>
        /// <seealso cref="Condition"/>
        public Condition Condition { get; set; }

        /// <summary>
        /// 温度の取得または設定をします。
        /// </summary>
        /// <remarks>
        /// DB上Nullが許可されており、Nullの場合Temperature自体がNullとなります。
        /// weater.Temperature.DisplayValueがNull例外となるため、
        /// 初期値にnew()を設定しています。
        /// </remarks>
        public Temperature Temperature { get; set; } = new();

        /// <summary>
        /// 都道府県の取得または設定をします。
        /// </summary>
        [ForeignKey(nameof(PrefectureId))]
        [InverseProperty("Weathers")]
        public virtual PrefectureEntity Prefecture { get; set; }

        public void Check()
        {
            if (PrefectureId == 0)
            {
                throw new ApplicationException("都道府県は入力必須です。");
            }
        }
    }
}