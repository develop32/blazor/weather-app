﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace WA.Shared.ValueObjects
{
    /// <summary>
    /// 状態の値オブジェクトクラス。
    /// </summary>
    public class Condition
    {
        /// <summary>不明</summary>
        public static readonly Condition None = new(0);

        /// <summary>晴れ</summary>
        public static readonly Condition Sunny = new(1);

        /// <summary>曇り</summary>
        public static readonly Condition Cloudy = new(2);

        /// <summary>雨</summary>
        public static readonly Condition Rain = new(3);

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="value">状態</param>
        public Condition(int value)
        {
            Value = value;
        }

        /// <summary>値</summary>
        [Column("Condition")]
        public int Value { get; set; }

        /// <summary>表示値</summary>
        public string DisplayValue
        {
            get
            {
                if (this.Value == Sunny.Value)
                {
                    return "晴れ";
                }

                if (this.Value == Cloudy.Value)
                {
                    return "曇り";
                }

                if (this.Value == Rain.Value)
                {
                    return "雨";
                }

                return "不明";
            }
        }

        /// <summary>
        /// 状態リストを返します。
        /// </summary>
        /// <returns>状態リスト</returns>
        public static IList<Condition> ToList()
        {
            List<Condition> conditionList = new()
            {
                new Condition(0),
                new Condition(1),
                new Condition(2),
                new Condition(3),
            };
            return conditionList;
        }
    }
}