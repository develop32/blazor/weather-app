﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WA.Shared.ValueObjects
{
    /// <summary>
    /// 温度の値オブジェクトクラス。
    /// </summary>
    public class Temperature
    {
        /// <summary>単位名</summary>
        public const string UnitName = "℃";

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="value">温度</param>
        public Temperature(decimal? value)
        {
            Value = value;
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public Temperature()
        {
        }

        /// <summary>値</summary>
        [Column("Temperature")]
        [Range(0, 99.999, ErrorMessage = "99.999以下を入力して下さい。")]
        public decimal? Value { get; set; }

        /// <summary>表示値</summary>
        public string DisplayValue
        {
            get
            {
                if (this.Value == null)
                {
                    return "不明";
                }

                return $"{this.Value?.ToString("0.00")} {UnitName}";
            }
        }
    }
}