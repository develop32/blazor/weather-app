﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WA.Shared.ValueObjects
{
    /// <summary>
    /// 天気日付の値オブジェクトクラス。
    /// </summary>
    public class WeatherDate
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="value">天気日付</param>
        public WeatherDate(DateTime? value)
        {
            Value = value;
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public WeatherDate()
        {
        }

        /// <summary>
        /// 値。<br />
        /// 時刻は必ず"00:00:00"です。
        /// </summary>
        [Display(Name = "日付")]
        [Required(ErrorMessage = "{0}を入力してください。")]
        [Column("WeatherDate", TypeName = "DATE")]
        public DateTime? Value { get; set; } = DateTime.Today;

        /// <summary>表示値</summary>
        public string DisplayValue
        {
            get
            {
                return Value?.ToString("yyyy/MM/dd");
            }
        }
    }
}