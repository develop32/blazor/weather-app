﻿using System;

namespace WA.Shared
{
    /// <summary>
    /// 天気予測エンティティクラス。
    /// </summary>
    public class WeatherForecast
    {
        /// <summary>
        /// 日付の取得または設定をします。
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// 温度℃の取得または設定をします。
        /// </summary>
        public int TemperatureC { get; set; }

        /// <summary>
        /// 要約または設定をします。
        /// </summary>
        public string Summary { get; set; }

        /// <summary>
        /// 温度Fの取得または設定をします。
        /// </summary>
        public int TemperatureF => 32 + (int)(this.TemperatureC / 0.5556);
    }
}